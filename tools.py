import re
from numpy import random
import pandas as pd
def toID(str):
    return re.sub(r'[^a-z0-9]', '', str.lower())
def randpull(l):
    n = len(l)-1
    i = random.binomial(2*n+1,0.5)
    if i>n:
        i=2*n+1-i
    pull = l.pop(i)
    l.insert(0,pull)
    return pull
def f2list(fname):
    with open(fname,'r') as f:
        l = f.read().splitlines()
    random.shuffle(l)
    return l
def loadnotol(room):
    notoldf = pd.read_csv(room+'notol.csv')
    notol = {}
    for u in notoldf['username']:
        notol[u]=0
    return notoldf, notol