from tools import toID, randpull
from importlib import reload
import time, os
import commands
import requests

def dc(bot, room, text, user, pm):
    bot.dc()

def c(bot, room, text, user, pm):
    if pm:
        if text[0] == '[' and ']' in text:
            e = text.index(']')
            room = text[1:e]
            text = text[e+1:].strip()
        else:
            text = '/w '+user+','+text
    bot.send(room, text)

def restart(bot, room, text, user, pm):
    bot.dc()
    os.system('python main.py')

def contime(bot, room, text, user, pm):
    pmtext = ''
    if pm:
        pmtext = '/pm '+user+','
    daytext = ''; hourtext = ''; minutetext = ''; secondtext = ''
    td = time.time() - bot.contime
    days = int(td/(24*60*60));  td-=days*24*60*60
    hours = int(td/(60*60)); td-=hours*60*60
    minutes = int(td/(60)); td-=minutes*60
    seconds = round(td, 2)
    if days!=0:
        daytext = str(days)+' days'
    if hours!=0:
        hourtext = str(hours)+' hours'
    if minutes!=0:
        minutetext = str(minutes)+' minutes'
    if seconds != 0:
        secondtext = str(seconds)+' seconds'
    bot.send(room, pmtext+'The bot has been connected for: '+(daytext+' '+hourtext+' '+minutetext+' '+secondtext).strip()+'.')

def repo(bot, room, text, user, pm):
    pmtext = ''
    if pm:
        pmtext = '/pm '+user+','
    bot.send(room, pmtext+"https://gitlab.com/XpRienzo/amphy-2000")

def hotpatch(bot, room, text, user, pm):
    #to be extended to include config, current implementation just a dummy.
    pmtext = ''
    if pm:
        pmtext = '/w '+user+', '
    bot.commands = reload(commands)
    bot.send(room, pmtext+'The commands have been hotpatched!')

def randtopic(bot, room, text, user, pm):
    if pm:
        return
    list = bot.lists['randtopics']
    bot.send(room, "/wall "+randpull(list))

def randcat(bot, room, text, user, pm):
    pmtext = ''
    if pm:
        pmtext = '/w '+user+', '
    r = requests.get('https://api.thecatapi.com/v1/images/search', headers={ 'x-api-key' : bot.cat_api_key })
    url = r.json()[0]['url']
    bot.send(room, pmtext+"!show "+url)

def randdog(bot, room, text, user, pm):
    pmtext = ''
    if pm:
        pmtext = '/w '+user+', '
    r = requests.get('https://api.thedogapi.com/v1/images/search', headers={ 'x-api-key' : bot.dog_api_key })
    url = r.json()[0]['url']
    bot.send(room, pmtext+"!show "+url)

def addnotol(bot, room, text, user, pm):
    t = text.split('|')
    rm=t[0]
    if(rm == ''):
        return
    bot.send(rm, "/cmd roominfo "+rm)
    t[1]=toID(t[1])
    al = []
    for rank in "%@#":
        if rank not in bot.rdict[rm].authlist:
            continue
        al+=bot.rdict[rm].authlist[rank]
    if user not in al:
        return
    if t[1] in bot.notol[rm]:
        bot.send(room, "/w "+user+", User already in the list")
        return
    bot.notoldf[rm].loc[len(bot.notoldf[rm])] = t[1:]
    bot.notol[rm][t[1]] = 0
    bot.notoldf[rm].to_csv(rm+'notol.csv',index=False)
    bot.send(rm, '/mn ['+t[1]+'] was added from the notol list by '+user)
    bot.send(room, "/w "+user+",List successfully updated!")

def removenotol(bot, room, text, user, pm):
    t = text.split('|')
    rm=t[0]
    if(rm == ''):
        return
    bot.send(rm, "/cmd roominfo "+rm)
    t[1]=toID(t[1])
    al = []
    for rank in "%@#":
        if rank not in bot.rdict[rm].authlist:
            continue
        al+=bot.rdict[rm].authlist[rank]
    if user not in al:
        return
    if t[1] not in bot.notol[rm]:
        bot.send(room, "/w "+user+", User not in the no tol list")
        return
    del bot.notol[rm][t[1]]
    bot.notoldf[rm]=bot.notoldf[rm][bot.notoldf[rm].username != t[1]]
    bot.notoldf[rm].to_csv(rm+'notol.csv',index=False)
    bot.send(rm, '/mn ['+t[1]+'] was removed from the notol list by '+user)
    bot.send(room, "/w "+user+",List successfully updated!")

def viewnotol(bot, room, text, user, pm):
    rm = toID(text)
    if(rm == ''):
        return
    bot.send(rm, "/cmd roominfo "+rm)
    al = []
    for rank in "%@#":
        if rank not in bot.rdict[rm].authlist:
            continue
        al+=bot.rdict[rm].authlist[rank]
    if user not in al:
        return
    htmlpage = bot.notoldf[rm].to_html(index=False)
    htmlpage = ''.join(htmlpage.split('\n'))
    print(htmlpage)
    bot.send(rm, '/pminfobox '+user+', '+htmlpage)

fdict = {'dc':dc, 'custom':c, 'c':c, 'contime':contime, 'uptime':contime, 'kill':dc, 'restart':restart, 'hotpatch':hotpatch, 'reload':hotpatch, 'git':repo, 'repo':repo, 'randtopic':randtopic, 'addnotol':addnotol, 'removenotol':removenotol, 'viewnotol':viewnotol, 'randcat':randcat, 'randdog':randdog}

devcoms = ['dc', 'custom', 'c', 'kill', 'restart', 'hotpatch', 'reload']

modcoms = ['addnotol','removenotol','viewnotol']

infocoms = ['uptime', 'contime', 'git', 'repo', 'randtopic', 'randcat', 'randdog']

ranks = {'uptime':'+', 'contime':'+', 'git':'+', 'repo':'+', 'randtopic':'+', 'randcat':'+','randdog':'+'}

def com(bot, room, text, user, command, pm=False):
    if toID(user) == toID(bot.usr['nickname']):
        return
    if command in modcoms:
        user=toID(user)
        fdict[command](bot,room,text,user,pm)
        return
    if command in devcoms and toID(user) in bot.devs:
        fdict[command](bot, room, text, user, pm)
        return
    if command in infocoms:
        prio = bot.ranks.index(ranks[command])
        if prio > bot.ranks.index(user[0]):
            fdict[command](bot, room, text, user, True)
        else:
            fdict[command](bot, room, text, user, pm)
