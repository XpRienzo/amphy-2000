import Bot
import tools
import structures
import time
import json
from collections import defaultdict

bot = Bot.Bot()

for f in bot.files:
    bot.lists[f]=tools.f2list(f+'.txt')

while bot.ws.connected:
    resp = bot.recv()
    print(resp+'\n')
    room = ''
    r_l = resp.split('\n')
    if len(r_l)!= 1:
        room = r_l[0][1:]
        opts = r_l[1].split('|')
    else:
        opts = r_l[-1].split('|')
    if len(opts) == 1:
        continue
    elif opts[1]=='updateuser':
        continue
    elif opts[1]=='init':
        bot.rdict[room] = structures.Room(resp)
        bot.send(room, "/cmd roominfo "+room)
        bot.notoldf[room], bot.notol[room] = tools.loadnotol(room)
    elif opts[1]=='J':
        bot.rdict[room].updateul(r_l[1])
    elif opts[1]=='L':
        bot.rdict[room].updateul(r_l[1])
    elif opts[1]=='N':
        bot.rdict[room].updateul(r_l[1])
    elif opts[1]=='c:':
        opts[4]='|'.join(opts[4:])
        if opts[4].split(' ')[0]=='/log':
            by = ' by '+opts[3][1:]
            p = opts[4].split(by)[0].split('was ')
            punishment = p[-1].split(' ')[0]
            if punishment not in ["muted", "warned", "banned"]:
                continue
            username = tools.toID('was'.join(p[0:-1])[5:])
            if username in bot.notol[room]:
                if (time.time() - bot.notol[room][username])/60 > 30:
                    bot.notol[room][username] = time.time()
                    bot.send(room, "/mn "+username+" is on the notol list.")
        elif opts[4][0] in bot.comchar:
            com = opts[4][1:].split(' ')[0]
            arg = ' '.join(opts[4][1:].split(' ')[1:])
            bot.commands.com(bot, room, arg, opts[3], com)
    elif opts[1]=='pm':
        opts[4]='|'.join(opts[4:])
        if opts[4].split(' ')[0]== '/botmsg':
            opts[4] = ' '.join(opts[4].split(' ')[1:])
        if opts[4][0] in bot.comchar:
            com = opts[4][1:].split(' ')[0]
            arg = ' '.join(opts[4][1:].split(' ')[1:])
            bot.commands.com(bot, room, arg, opts[2], com, True)
    elif opts[1]=='queryresponse' and opts[2]=='roominfo':
        rinfo = json.loads(opts[-1])
        bot.rdict[rinfo['id']].authlist = rinfo['auth']